package routers

import (
	"install/controllers"
	"github.com/astaxie/beego"
)

func init() {
    //beego.Router("/", &controllers.InstallController{})
	beego.Router("/protocol", &controllers.InstallController{},"*:Protocol")
	beego.Router("/environment", &controllers.InstallController{},"*:Environment")
	beego.Router("/baseService", &controllers.LongPollingController{},"get:Join")
	beego.Router("/test", &controllers.InstallController{},"*:Test")
	beego.Router("/list", &controllers.InstallController{}, "*:List")
	beego.Router("/finish", &controllers.InstallController{}, "*:Finishs")
	// Register routers.
	beego.Router("/", &controllers.AppController{})
	// Indicate AppController.Join method to handle POST requests.
	beego.Router("/join", &controllers.AppController{}, "post:Join")

	// Long polling.
	beego.Router("/lp", &controllers.LongPollingController{}, "get:Join")
	beego.Router("/lp/post", &controllers.LongPollingController{})
	beego.Router("/lp/fetch", &controllers.LongPollingController{}, "get:Fetch")

	// WebSocket.
	beego.Router("/ws", &controllers.WebSocketController{})
	beego.Router("/ws/join", &controllers.WebSocketController{}, "get:Join")
}
